var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var db = require('./models/db');
var controllers = require('./controller/index');
var morgan  = require('morgan');
var feed;

io.on('connection', function(socket){
    feed = require('./models/feeds')(socket); 
})

var dbModel = new db();

dbModel.setupDb();
app.use(morgan('dev'));  
app.use(bodyParser.json());
app.use(express.static(__dirname + '/views'));
app.use(controllers);

http.listen(3000, () => {
    console.log('listening on port 3000');
})