var express = require('express');
var router = express.Router();

var pollModel = require('../models/polls');

router.route('/')
    .get(function(req, res){
        var pollObj = new pollModel();
        pollObj.getAllPolls(function(err, pollResponse){
            if(err) return res.json({responseCode: 1, ResponseDesc: pollResponse});
            res.json({responseCode: 0, ResponseDesc: 'Success', data: pollResponse});
        });
    })
    .post(function(req, res){
        //code to add new polls
        var pollObj = new pollModel();
        pollObj.addNewPolls(req.body, function(err, pollResponse){
            if(err) return res.json({responseCode: 1, ResponseDesc: pollResponse});
            res.json({responseCode: 0, ResponseDesc: 'Success', data: pollResponse});
        }); 
    })
    .put(function(req, res){
        //code to update votes of poll
        var pollObj = new pollModel();
        pollObj.votePollOption(req.body, function(err, pollResponse){
            if(err) return res.json({responseCode: 1, ResponseDesc: pollResponse});
            res.json({responseCode: 0, ResponseDesc: 'Success', data: pollResponse});
        });
    });


module.exports = router;